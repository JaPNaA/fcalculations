const WEB_PATH = "web";
const BUILD_PATH = "public";
const PAGES_PATH = "pages";
const PAGE_FILE_EXT = ".js";
const HTML_PAGE_FILE_TEMPLATE_PATH = "web/_pageTemplate.html";

const fs = require("fs");
const path = require("path");
const childProcess = require("child_process");

try { childProcess.execSync(`rm -rd ${BUILD_PATH}`); } catch (e) { }
childProcess.execSync(`cp -r ${WEB_PATH} ${BUILD_PATH}`);

const pagesFiles = fs.readdirSync(`${BUILD_PATH}/${PAGES_PATH}`);
const htmlPageFileTemplate = fs.readFileSync(HTML_PAGE_FILE_TEMPLATE_PATH).toString();

for (const pageFile of pagesFiles) {
    if (!pageFile.endsWith(PAGE_FILE_EXT)) { continue; }
    const pageName = pageFile.slice(0, -PAGE_FILE_EXT.length);

    fs.writeFileSync(
        `${BUILD_PATH}/${pageName}.html`,
        createPageFromTemplate({
            title: camelCaseToEnglish(pageName),
            pageName: pageName
        })
    );
}

function createPageFromTemplate({ title, pageName }) {
    return htmlPageFileTemplate
        .replace(/{{title}}/g, title)
        .replace(/{{pageName}}/g, pageName);
}

/** @param {string} str */
function camelCaseToEnglish(str) {
    return str.slice(0, 1).toUpperCase() +
        str.slice(1).split(/(?=[A-Z])/g).join(" ");
}
