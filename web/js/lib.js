class Elm {
    constructor() {
        this.elm = document.createElement("div");
    }

    /** @param {HTMLElement | Elm} parent */
    appendTo(parent) {
        if (parent instanceof Elm) { parent.append(this); }
        else { parent.appendChild(this.elm); }
        return this;
    }

    /** @param {string} name */
    class(name) {
        this.elm.classList.add(name);
        return this;
    }

    /** @param {...Elm} elms */
    append(...elms) {
        for (const elm of elms) {
            this.elm.appendChild(elm.elm);
        }
        return this;
    }
}

class PageContent {
    //
}

class Calculator extends Elm {
    constructor() {
        super();

        this.elm = document.createElement("div");

        /** @type {AbstractVariableInput[]} */
        this.inputs = [];

        /**
         * Oldest first
         * @type {AbstractVariableInput[]}
         */
        this._changedInputs = [];
    }

    /**
     * @param {AbstractVariableInput} input 
     */
    alertInputChanged(input) {
        const existingIndex = this._changedInputs.indexOf(input);
        if (existingIndex >= 0) { this._changedInputs.splice(existingIndex, 1); }
        this._changedInputs.push(input);

        console.log(this._changedInputs.length);

        this.update();
    }

    update() {
        const modifiableInputs = this._updateAndGetModifiableInputs();

        for (const modifiableInput of modifiableInputs) {
            try {
                const result = modifiableInput.tryToSolveForSelf();
                modifiableInput.setDisplayValue(result);
            } catch (e) {
                console.log("Can't solve");
            }
        }
    }

    _updateAndGetModifiableInputs() {
        const modifiableInputs = this.inputs.filter(e => (
            !this._changedInputs.includes(e) ||
            e.modifiable === AbstractVariableInput.MODIFIABLE_ALWAYS
        ) && e.modifiable !== AbstractVariableInput.MODIFIABLE_NEVER);

        if (modifiableInputs.length === 0) {
            modifiableInputs.push(this._shiftOldestModifiableInput());
        }

        return modifiableInputs;
    }

    _shiftOldestModifiableInput() {
        for (let i = 0; i < this._changedInputs.length; i++) {
            if (this._changedInputs[i].modifiable !== AbstractVariableInput.MODIFIABLE_NEVER) {
                return this._changedInputs.splice(i, 1)[0];
            }
        }
    }
}

/**
 * @typedef VariableInputOptions
 * @property {string} [name]
 * @property {number} [modifiable]
 * @property {boolean} [canChangeModifiable]
 */

class AbstractVariableInput extends Elm {
    /**
     * @param {Calculator} parentCalculator
     * @param {VariableInputOptions} [options]
     */
    constructor(parentCalculator, options = {}) {
        super();

        this.class("variable");
        if (options.name) { this.elm.classList.add(options.name); }

        this.parentCalculator = parentCalculator;

        /** @type {Variable[]} */
        this.variables = [];


        this.modifiable = options.modifiable || AbstractVariableInput.MODIFIABLE_AUTO;
        if (options.canChangeModifiable) {
            this._appendModifiableToggle();
        }
    }

    newVar() {
        const variable = new Variable();
        this.variables.push(variable)
        return variable;
    }

    /** @param {number} value */
    setDisplayValue(value) {
        throw new Error("Not implemented");
    }

    /** @param {number | string} value */
    updateValue(value) {
        let numberValue;
        if (typeof value === 'string') {
            numberValue = parseFloat(value);
        } else {
            numberValue = value;
        }

        for (const variable of this.variables) {
            variable.setKnownValue(numberValue);
        }
        this.parentCalculator.alertInputChanged(this);
    }

    tryToSolveForSelf() {
        /** @type { undefined | number } */
        let value = undefined;

        for (const variable of this.variables) {
            const solve = variable.solveForSelf();
            try {
                value = solve.eval();
                break;
            } catch (e) { }
        }

        if (value === undefined) {
            throw new CantEvaluateException();
        } else {
            return value;
        }
    }

    _appendModifiableToggle() {
        throw new Error("Not implemented");
    }
}

AbstractVariableInput.MODIFIABLE_AUTO = 0;
AbstractVariableInput.MODIFIABLE_ALWAYS = 1;
AbstractVariableInput.MODIFIABLE_NEVER = 2;


class VariableInput extends AbstractVariableInput {
    /**
     * @param {Calculator} parentCalculator
     * @param {VariableInputOptions} [options]
     */
    constructor(parentCalculator, options) {
        super(parentCalculator, options);

        this.input = document.createElement("input");
        this.input.type = "number";
        this.elm.appendChild(this.input);

        this.input.addEventListener("input", () => {
            this.updateValue(this.input.value);
        });
    }

    /** @param {number} value */
    setDisplayValue(value) {
        this.input.value = value.toString();
    }
}

class SelectVariableInput extends AbstractVariableInput {
    /**
     * @param {Calculator} parentCalculator
     * @param {{ name?: string, options: { [x: string]: number } }} options
     */
    constructor(parentCalculator, options) {
        super(parentCalculator, {
            ...options,
            modifiable: AbstractVariableInput.MODIFIABLE_NEVER
        });

        this.input = document.createElement("select");

        const keys = Object.keys(options.options);
        for (const key of keys) {
            const option = document.createElement("option");
            option.text = key;
            option.value = options.options[key].toString();
            this.input.appendChild(option);
        }

        this.elm.appendChild(this.input);
        this.input.addEventListener("change", () =>
            this.updateValue(this.input.value)
        );
        setTimeout(() =>
            this.updateValue(this.input.value),
            1
        );
    }
}
