/**
 * @typedef Page
 * @property {string} name
 * @property {string} link
 * @property {string} [color]
 */
/**
 * @type {Page[]}
 */
const pages = [{
    name: "Three Kingdoms Currency Converter",
    link: "threeKingdoms",
    color: "#e6541b"
}, {
    name: "Ruru Punishment",
    link: "ruruPunishment",
    color: "#aef187"
}];

class IndexPage extends Elm {
    constructor() {
        super();
        this.class("indexPage");

        this.append(
            new Elm().class("left").append(
                new Elm().class("titlePositionBox").append(
                    new Elm().class("titleContainer").append(
                        this.titleElm = new Elm().class("title")
                    )
                )
            ),
            new Elm().class("right").append(
                this.calculatorsListElm = new Elm().class("calculatorsList")
            )
        );

        this.titleElm.elm.innerText = "fcalculations";

        for (const page of pages) {
            this.calculatorsListElm.append(
                new IndexCalculatorLink(page)
            );
        }
    }
}

class IndexCalculatorLink extends Elm {
    /**
     * @param {Page} page
     */
    constructor(page) {
        super();

        this.class("calculatorLink");
        if (page.color) {
            this.elm.style.backgroundColor = page.color;
            if (isColorDark(page.color)) {
                this.class("darkBackground");
            }
        }

        this.link = document.createElement("a");
        this.link.innerText = page.name;
        this.link.href = page.link;

        this.elm.appendChild(this.link);
    }
}

/** @param {string} color */
function isColorDark(color) {
    const r = parseInt(color.substr(1, 2), 16);
    const g = parseInt(color.substr(3, 2), 16);
    const b = parseInt(color.substr(5, 2), 16);

    return r * 0.299 + g * 0.587 + b * 0.114 < 186;
}

main.append(new IndexPage());
