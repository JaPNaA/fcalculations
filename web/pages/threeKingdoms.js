const currencyOptions = {
    "cad": 1,
    "\u0416": 1.46,
    "\u5344": 1 / 2300,
    "\u0586": 15 / 23
};

class CurrencyExchangeCalculator extends Calculator {
    constructor() {
        super();

        const currA = new VariableInput(this, {
            name: "currA"
        });
        const currB = new VariableInput(this, {
            name: "currB"
        });
        const exchangeRateA = new SelectVariableInput(this, {
            name: "rateA",
            options: currencyOptions
        });
        const exchangeRateB = new SelectVariableInput(this, {
            options: currencyOptions,
            name: "rateB"
        });

        new Expression("subtract",
            new Expression("multiply",
                exchangeRateA.newVar(), currA.newVar(),
            ),
            new Expression("multiply",
                exchangeRateB.newVar(), currB.newVar()
            )
        );

        this.inputs.push(currA, currB, exchangeRateA, exchangeRateB);

        this.append(
            new Elm().class("a").class("row").append(
                currA, exchangeRateA
            ),
            new Elm().class("b").class("row").append(
                currB, exchangeRateB
            )
        );
    }
}

main.append(new CurrencyExchangeCalculator());
